#!/bin/bash
# mlflow server --default-artifact-root=file:/path/to/artifacts --host 0.0.0.0 --port 
mlflow mlflow server --backend-store-uri postgresql://unicorn_user:magical_password@0.0.0.0/rainbow_database
                        --default-artifact-root s3://mlopsdvc
                        --host 0.0.0.0
                        --port 5000
